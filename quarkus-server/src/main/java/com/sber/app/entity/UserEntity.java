package com.sber.app.entity;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;

/**
 * Persistent entity for User object.
 */
@MongoEntity(collection = "users")
public class UserEntity extends PanacheMongoEntity
{
	private String username;
	private String phoneNumber;
	private String country;
	private String city;
	private String dateOfBirthday;
	private String skype;
	private String site;
	private String education;

	/**
	 * Default constructor, used by Mongo.
	 */
	public UserEntity()
	{
	}

	/**
	 * Constructor.
	 * @param username
	 * 	username for creating a new User object
	 */
	public UserEntity(String username)
	{
		this.username = username;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getDateOfBirthday()
	{
		return dateOfBirthday;
	}

	public void setDateOfBirthday(String dateOfBirthday)
	{
		this.dateOfBirthday = dateOfBirthday;
	}

	public String getSkype()
	{
		return skype;
	}

	public void setSkype(String skype)
	{
		this.skype = skype;
	}

	public String getSite()
	{
		return site;
	}

	public void setSite(String site)
	{
		this.site = site;
	}

	public String getEducation()
	{
		return education;
	}

	public void setEducation(String education)
	{
		this.education = education;
	}

	/**
	 * Find user by username.
	 * @param username
	 * 	username for finding
	 * @return found user.
	 */
	public static UserEntity findByUsername(String username)
	{
		return find(User.USERNAME_FIELD, username).firstResult();
	}
}
