package com.sber.app.resource;

import java.util.HashMap;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import com.sber.app.service.UserService;

/**
 * Resource class contains methods for HTTP endpoints.
 */
@Path("/profile")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource
{
	private static final Logger LOG = Logger.getLogger(UserResource.class);
	private static final String USERNAME_PARAMETER = "username";
	private static final String REQUEST_EVENT_MAP_KEY = "event";
	private static final String EVENT_DATA_MAP_KEY = "data";
	private static final String DATA_VALUES_MAP_KEY = "new";
	private static final String FAILED_TO_GET_USERNAME_FROM_HASURA_REQUEST = "Failed to get username from Hasura request.";

	@Inject
	UserService userService;

	/**
	 * Method for creating a new user with a given name.
	 * @param request
	 * 	Hasura request, caused by Keycloak when creating a new user
	 */
	@POST
	@Path("/create")
	public void create(HashMap<String, Object> request)
	{
		String username = (String) getUsernameParameterFromHasuraRequest(request);
		if (username != null)
		{
			userService.createUser(username);
		}
	}

	private static Object getUsernameParameterFromHasuraRequest(HashMap<String, Object> request)
	{
		try
		{
			HashMap<String, Object> requestEvent = (HashMap<String, Object>) request.get(REQUEST_EVENT_MAP_KEY);
			HashMap<String, Object> eventData = (HashMap<String, Object>) requestEvent.get(EVENT_DATA_MAP_KEY);
			HashMap<String, Object> dataValues = (HashMap<String, Object>) eventData.get(DATA_VALUES_MAP_KEY);
			return dataValues.get(USERNAME_PARAMETER);
		}
		catch (NullPointerException e)
		{
			LOG.error(FAILED_TO_GET_USERNAME_FROM_HASURA_REQUEST);
			return null;
		}
	}
}
