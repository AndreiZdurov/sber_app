package com.sber.app.resource;

import javax.inject.Inject;

import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Mutation;
import org.eclipse.microprofile.graphql.Name;
import org.eclipse.microprofile.graphql.Query;

import com.sber.app.entity.UserEntity;
import com.sber.app.service.UserService;

/**
 * Resource class contains methods for GraphQL requests.
 */
@GraphQLApi
public class GqlUserResource
{
	@Inject
	UserService userService;

	/**
	 * Method for getting user profile by username.
	 * @param username
	 * 	username for finding
	 * @return found user.
	 */
	@Query("getProfile")
	@Description("Get user by username")
	public UserEntity getProfile(@Name("username") String username)
	{
		return userService.getUserByUsername(username);
	}

	/**
	 * Method for updating the user.
	 * @param user
	 * 	user to update
	 * @return updated user.
	 */
	@Mutation
	@Description("Update user")
	public UserEntity updateProfile(UserEntity user)
	{
		return userService.updateUser(user);
	}
}
