package com.sber.app.service;

import javax.enterprise.context.ApplicationScoped;

import org.jboss.logging.Logger;

import com.sber.app.entity.UserEntity;

/**
 * The service provides methods for working with the User entity.
 */
@ApplicationScoped
public class UserService
{
	private static final Logger LOG = Logger.getLogger(UserService.class);
	private static final String USER_CREATED_LOG_MESSAGE = "User successfully created: ";
	private static final String USER_FOUND_LOG_MESSAGE = "User found with name: ";
	private static final String USER_NOT_FOUND_LOG_MESSAGE = "User not found with specified name: ";
	private static final String USER_UPDATED_LOG_MESSAGE = "User updated: ";

	/**
	 * Method for user creation.
	 * @param username
	 * 	user which should created
	 * @return created user.
	 */
	public UserEntity createUser(String username)
	{
		UserEntity userEntity = new UserEntity(username);
		userEntity.persist();
		LOG.info(USER_CREATED_LOG_MESSAGE + username);
		return userEntity;
	}

	/**
	 * Method for getting user by username.
	 * @param username
	 * 	username for finding
	 * @return found user.
	 */
	public UserEntity getUserByUsername(String username)
	{
		UserEntity user = UserEntity.findByUsername(username);
		if (user == null)
		{
			LOG.info(USER_NOT_FOUND_LOG_MESSAGE);
			return null;
		}
		LOG.info(USER_FOUND_LOG_MESSAGE + user.getUsername());
		return user;
	}

	/**
	 * Method for updating the user.
	 * @param user
	 * 	user to update
	 * @return updated user.
	 */
	public UserEntity updateUser(UserEntity user)
	{
		UserEntity userEntity = UserEntity.findByUsername(user.getUsername());
		if (userEntity == null) {
			LOG.error(USER_NOT_FOUND_LOG_MESSAGE + user.getUsername());
			return null;
		}
		user.id = userEntity.id;
		user.update();
		LOG.info(USER_UPDATED_LOG_MESSAGE + user.getUsername());
		return user;
	}
}
