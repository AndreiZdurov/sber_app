package com.sber.app.service;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.sber.app.entity.UserEntity;
import com.sber.app.mongo.MongoDbContainer;

import io.quarkus.test.junit.QuarkusTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The <code>UserServiceTest</code> provides methods to test the {@link UserService} class.
 */
@Testcontainers
@QuarkusTest
class UserServiceTest
{
	/**
	 * Init test mongo container.
	 */
	@Container
	static GenericContainer<MongoDbContainer> MONGO_DB_CONTAINER = new MongoDbContainer();

	@Inject
	UserService userService;

	/**
	 * Test for {@link UserService#createUser(String)} method. Checks the creation of a user with the given name.
	 */
	@Test
	void createUser()
	{
		String username = "UserForCreateUserMethod";
		UserEntity createdUser = userService.createUser(username);
		assertThat(createdUser.getUsername()).isEqualTo(username);
	}

	/**
	 * Test for {@link UserService#getUserByUsername(String)} method. Checks the loading of a user by the given name.
	 */
	@Test
	void getUserByUsername()
	{
		String username = "UserForGetUserMethod";
		userService.createUser(username);

		UserEntity foundUser = userService.getUserByUsername(username);
		assertThat(foundUser.getUsername()).isEqualTo(username);
	}

	/**
	 * Test for {@link UserService#updateUser(UserEntity)} method.
	 */
	@Test
	void updateUser()
	{
		String username = "UserForUpdateUserMethod";
		String phoneNumber = "+7(000)123-45-67";
		String country = "Japan";
		String city = "Tokyo";
		String dateOfBirthday = "1970-01-01";
		String skype = "skypeName";
		String site = "www.test.com";
		String education = "primary";
		UserEntity createdUser = userService.createUser(username);
		createdUser.setPhoneNumber(phoneNumber);
		createdUser.setCountry(country);
		createdUser.setCity(city);
		createdUser.setDateOfBirthday(dateOfBirthday);
		createdUser.setSkype(skype);
		createdUser.setSite(site);
		createdUser.setEducation(education);
		userService.updateUser(createdUser);

		UserEntity foundUser = userService.getUserByUsername(username);
		assertThat(foundUser.getUsername()).isEqualTo(username);
		assertThat(foundUser.getPhoneNumber()).isEqualTo(phoneNumber);
		assertThat(foundUser.getCountry()).isEqualTo(country);
		assertThat(foundUser.getCity()).isEqualTo(city);
		assertThat(foundUser.getDateOfBirthday()).isEqualTo(dateOfBirthday);
		assertThat(foundUser.getSkype()).isEqualTo(skype);
		assertThat(foundUser.getSite()).isEqualTo(site);
		assertThat(foundUser.getEducation()).isEqualTo(education);
	}
}
