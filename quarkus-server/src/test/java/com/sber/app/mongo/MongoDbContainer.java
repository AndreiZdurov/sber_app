package com.sber.app.mongo;

import org.testcontainers.containers.GenericContainer;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;

public class MongoDbContainer extends GenericContainer<MongoDbContainer>
{
	private static final String DEFAULT_IMAGE_AND_TAG = "mongo:3.6.21-xenial";
	private static final int MONGODB_PORT = 27017;
	private static final String MONGODB_HOST = "localhost";

	public MongoDbContainer()
	{
		super(DEFAULT_IMAGE_AND_TAG);
		PortBinding portBinding = new PortBinding(Ports.Binding.bindPort(MONGODB_PORT), new ExposedPort(MONGODB_PORT));
		withCreateContainerCmdModifier(cmd -> cmd.withHostName(MONGODB_HOST).withPortBindings(portBinding));
	}
}