# Test project "SBER-APP"

Structure of the project modules:
![alt text](./docs/modules_diagram.png)

## How to launch:

1. Need to change all variables with value _your_public_ip_ on your local machine ip address This variables exists in next files:
    - _docker-compose.yml_
    - _react-client/.env_
    - _quarkus-server/src/main/resources/application.properties_
2. In the folder _react-client_ execute next commands:
    - `npm install` (or yarn install)
    - `npm run build` (or yarn build)
3. Build a container image for Quarkus server. In the folder _quarkus-server_ execute one of next commands:
    - For Linux: `mvn clean package -Dquarkus.container-image.build=true -DskipTests=true`
    - For Windows: `mvn clean package "-Dquarkus.container-image.build=true" "-DskipTests=true"`

Need to change variable with value _user_name_ on your computer username. This used in the name of created docker container.

You can see the name of the container in the screenshot.
![alt text](./docs/quarkus-container-name.png)
Change _user_name_ value in _docker-compose.yml_, like here:
![alt text](./docs/quarkus-docker-compose.png)

4. Launch all docker containers by executing command:
    - `docker-compose up`
5. Need to connect to Postgres database and check existing _keycloak_ schema. If it don't created automatically, create it manual.
   After this restart docker containers.
6. Keycloak configuration. Open `localhost: 8081`, create new realm with name
   _sber-app_, import backup config file _realm-export.json_ from the root of the project.

   After this should to be configured sections: Realm, Clients, Roles, User Group.
7. Hasura configuration. Open `localhost: 8082`, go to setting, click on _import_
   and choose _hasura_metadata.json_ from the root of the project.
8. After successful configuration open `localhost:3000` with React client.
