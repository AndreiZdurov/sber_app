const CracoLessPlugin = require('craco-less');

module.exports = {
	plugins: [
		{
			plugin: CracoLessPlugin,
			options: {
				lessLoaderOptions: {
					lessOptions: {
						modifyVars: {
							'@primary-color': '#06c',
							'@font-family': '"Open Sans", sans-serif',
						},
						javascriptEnabled: true,
					},
				},
			},
		},
	],
}