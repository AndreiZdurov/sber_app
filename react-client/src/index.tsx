import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { keycloak, initializeKeycloak } from './auth/keycloak';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/react-hooks';

const run = async () => {
    await initializeKeycloak();
    await keycloak.loadUserInfo();
    const client = new ApolloClient({
        uri: `${process.env.REACT_APP_GRAPHQL_ENDPOINT}`,
        cache: new InMemoryCache(),
        headers: {
            Authorization: `Bearer ${keycloak.token}`,
        },
    });

    ReactDOM.render(
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>
        ,document.getElementById("root")
    );
};

run();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
