export interface User {
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    city: string;
    country: string;
    dateOfBirthday: any;
    education: string;
    phoneNumber: string;
    site: string;
    skype: string;
    prevState?: null;
}