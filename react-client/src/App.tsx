import React from 'react';
import './App.less';
import Profile from './components/Profile';
import { keycloak } from './auth/keycloak';
import Header from './components/Header';

function App() {
    const userInfo: any = keycloak.userInfo;

    return (
        <React.Fragment>
            <Header/>
            <Profile username={userInfo.preferred_username}/>
        </React.Fragment>
  );
}

export default App;
