import Keycloak from 'keycloak-js';

export const keycloak = Keycloak({
	realm: `${process.env.REACT_APP_KEYCLOAK_REALM}`,
	url: `${process.env.REACT_APP_KEYCLOAK_URL}`,
	clientId: `${process.env.REACT_APP_KEYCLOAK_CLIENT_ID}`,
});

export const initializeKeycloak = async () => {
	return keycloak.init({
		onLoad: "login-required",
	});
};