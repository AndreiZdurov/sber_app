import React from 'react';
import './style.css';
import { keycloak } from '../../auth/keycloak';
import { Button } from 'antd';

function Header() {
	return (
		<div>
			<div className='header-btn-container'>
				<Button type='primary'
						danger={true}
						onClick={() => keycloak.logout()}
				>Logout</Button>
			</div>
			<div className='header-title'>SBER-APP</div>
		</div>
	);
}

export default Header;