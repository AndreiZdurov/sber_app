import React from 'react';
import './style.css';
import { Spin } from 'antd';

interface ComponentProps {
    spinning: boolean;
}

function Spinner({spinning}: ComponentProps) {
    return (
        <div className='spinner-container'>
            <Spin spinning={spinning} size='large' />
        </div>
    );
}

export default Spinner;