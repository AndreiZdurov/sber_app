import React from 'react';
import './style.css';
import { User } from '../../model/User';

interface ComponentProps {
    user: User;
}

function ProfileInfo({user}: ComponentProps) {
    return(
        <div className='profile-info'>
            <div><strong>User name: </strong>{user.username}</div>
            <div><strong>Email: </strong>{user.email}</div>
            <div><strong>First name: </strong>{user.first_name}</div>
            <div><strong>Last name: </strong>{user.last_name}</div>
            <div><strong>Phone number: </strong>{user.phoneNumber ? user.phoneNumber : ''}</div>
            <div><strong>Skype: </strong>{user.skype ? user.skype : ''}</div>
            <div><strong>Website: </strong>{user.site ? user.site : ''}</div>
            <div><strong>Date of birth: </strong>{user.dateOfBirthday ? user.dateOfBirthday : ''}</div>
            <div><strong>Country: </strong>{user.country ? user.country : ''}</div>
            <div><strong>City: </strong>{user.city ? user.city : ''}</div>
            <div><strong>Education: </strong>{user.education ? user.education : ''}</div>
        </div>
    );
}

export default ProfileInfo;