import { gql } from '@apollo/client';

export const GET_USER = gql`
    query MyQuery($username: String!) {
        keycloak_user_entity(where: {username: {_eq: $username}}) {
            first_name
            last_name
            email
            username
        }
        getProfile(username: $username) {
            city
            country
            dateOfBirthday
            education
            phoneNumber
            site
            skype
            username
        }
    }
`;

export const UPDATE_USER = gql`
    mutation MyMutation(
        $city: String,
        $country: String,
        $dateOfBirthday: String,
        $education: String,
        $phoneNumber: String,
        $site: String,
        $username: String,
        $first_name: String,
        $last_name: String,
        $skype: String,
    ) {
        updateProfile( user: {
            city: $city,
            country: $country,
            dateOfBirthday: $dateOfBirthday,
            education: $education,
            phoneNumber: $phoneNumber,
            site: $site,
            skype: $skype,
            username: $username
        }
        ) {
            city
            country
            dateOfBirthday
            education
            phoneNumber
            site
            skype
            username
        }
        update_keycloak_user_entity(where: {username: {_eq: $username}}, _set: {first_name: $first_name, last_name: $last_name}) {
            returning {
                last_name
                first_name
            }
        }
    }
`;