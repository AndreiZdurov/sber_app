import React, { useEffect, useState } from 'react';
import './style.css';
import { useQuery, useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { Button, DatePicker, Form, Input, message, Alert } from 'antd';
import { GET_USER, UPDATE_USER } from './queries';
import Spinner from '../Spinner';
import ProfileInfo from '../ProfileInfo';
import { User } from '../../model/User';

const { TextArea } = Input;

interface ComponentProps {
	username: string;
}

function Profile({username}: ComponentProps) {
	const { loading, error, data } = useQuery(GET_USER, {variables: {username: username}});
	const [user, setUser] = useState<User | null>(null);
	const [isEditing, setIsEditing] = useState<boolean>(false);
	const [form] = Form.useForm();
	const [updateUser, updateUserResult] = useMutation(UPDATE_USER);

	useEffect(() => {
		if(data) {
			setUser({
				...data.getProfile,
				...data.keycloak_user_entity[0],
			});
		}
	}, [data]);

	const onFinish = (formValues: User) => {
		const parsedValues: User = {
			...formValues,
			'dateOfBirthday': formValues['dateOfBirthday'] ? formValues['dateOfBirthday'].format('YYYY-MM-DD') : null,
		};
		updateUser({variables: {...parsedValues}})
			.then(() => {
				setUser(parsedValues);
				setIsEditing(false);
			})
			.catch(error => message.error(error.message));
	};

	const onReset = () => {
		form.resetFields();
		setIsEditing(false);
	};

	return (
		<div className='profile-card'>
			<div className='profile-title'>User profile info</div>
			<Spinner spinning={loading} />
			{error &&
				<Alert
					message="Error"
					description={error.message}
					type="error"
					showIcon
				/>
			}
			{!isEditing && user &&
				<div>
					<ProfileInfo user={user} />
					<div className='profile-btn-container'>
						<Button className='profile-btn' type='primary' onClick={() => setIsEditing(true)}>Edit</Button>
					</div>
				</div>
			}
			{isEditing && user &&
				<Form	layout='vertical'
						form={form}
						initialValues={{
							...user,
							'dateOfBirthday': user.dateOfBirthday ? moment(user.dateOfBirthday) : null,
						}}
						onFinish={onFinish}
						onReset={onReset}
				>
					<Form.Item name='username' label='User name' rules={[{required: true}]}>
						<Input className='form-input' disabled={true} />
					</Form.Item>
					<Form.Item name='email' label='Email' rules={[{required: true}]}>
						<Input className='form-input' disabled={true} />
					</Form.Item>
					<Form.Item name='first_name' label='First name' rules={[{required: true}]}>
						<Input className='form-input' />
					</Form.Item>
					<Form.Item name='last_name' label='Last name' rules={[{required: true}]}>
						<Input className='form-input' />
					</Form.Item>
					<Form.Item name='phoneNumber' label='Phone number'>
						<Input className='form-input' placeholder='Enter your phone number' />
					</Form.Item>
					<Form.Item name='skype' label='Skype'>
						<Input className='form-input' placeholder='Enter your skype' />
					</Form.Item>
					<Form.Item name='site' label='Website'>
						<Input className='form-input' placeholder='Enter your website' />
					</Form.Item>
					<Form.Item name='dateOfBirthday' label='Date of birth'>
						<DatePicker style={{display: 'block'}} />
					</Form.Item>
					<Form.Item name='country' label='Country'>
						<Input className='form-input' placeholder='Enter your country' />
					</Form.Item>
					<Form.Item name='city' label='City'>
						<Input className='form-input' placeholder='Enter your city' />
					</Form.Item>
					<Form.Item name='education' label='Education'>
						<TextArea rows={4} />
					</Form.Item>
					<div className='profile-btn-container'>
						<Button className='profile-btn'
							type="primary"
							htmlType="submit"
							loading={updateUserResult.loading}
						>
							Submit
						</Button>
						<Button className='profile-btn' onClick={onReset}>
							Cancel
						</Button>
					</div>
				</Form>
			}
		</div>
	);
}

export default Profile;